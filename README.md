# Laravel Squareup API Package

> Laravel Squareup API Package enables secure payment processing and easy integration with Square's suite of business solutions. Simplify order management, inventory synchronization, and customer record-keeping, ensuring seamless interaction with Square sellers' systems. Easily build dynamic business applications with Square's RESTful API design.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
    - [Setup .env file](#setup-env-file)
    - [API](#API)
    - [Webhook](#webhook)
- [Support](#support)
- [Donate](#donate)

## Installation

Via composer (Laravel 10+):

```shell
composer require mahan-shoghy/laravel-squareup
```

Publish the config file, use the following command:

```shell
php artisan vendor:publish --provider="MahanShoghy\LaravelSquareup\SquareupServiceProvider" --tag="config"
```

## Usage

### Setup .env file

Add and set these variables to your environment file:
```dotenv
SQUAREUP_APPLICATION_ID=
SQUAREUP_APPLICATION_SECRET=
SQUAREUP_ACCESS_TOKEN=
```

You can obtain these values from the
[Squareup App Setting](https://developer.squareup.com/apps).

If you wish to use the sandbox, set
`SQUAREUP_HOST` to
`https://connect.squareupsandbox.com`
in your environment file.

### API

Explore the various API endpoints provided by the Squareup API, organized into the following categories:

- Essentials
  - [OAuth](src/App/Essentials/OAuth/README.md) 
  - [Webhook Subscription](src/App/Essentials/WebhookSubscriptions/README.md) 
- Commerce
  - [Orders](src/App/Commerce/Orders/README.md) 
- Merchants
  - [Locations](src/App/Merchants/Locations/README.md)

Each section provides detailed instructions on how to interact with the Squareup API for different aspects of your business.

### Webhook

Webhooks allow your Laravel application to receive and handle Squareup events in real-time. Follow these steps to set up webhook handling in your application.

1. **Define a Webhook Route**: In your Laravel routes file (like: web.php), add a route for handling webhooks. You can choose any URL you prefer:
    ```php
    Route::squareupWebhooks('your-webhook-url');
    ```
2. **Configure Webhook Events**: Open the `squareup.php` configuration file (which you published in [Installation](#installation)) and specify the events you want to handle and the corresponding job classes. Here's an example:
    ```php
    'jobs' => [
        'bank_account_created' => BankAccountCreatedJob::class
    ]
    ```
   In the job class, you can access the payload data sent by Squareup to handle the event.
    ```php
    class BankAccountCreatedJob implements ShouldQueue
    {
        public array $payload;
    
        public function __construct(array $payload)
        {
            $this->payload = $payload;
        }
    
        public function handle(): void
        {
            // Handle the webhook event using $this->payload
        }
    }
    ```
3. **Event Documentation**: You can find a list of available Squareup webhook events in the
   [Squareup documentation](https://developer.squareup.com/reference/square/webhooks).
   This resource will help you understand and configure the events you wish to handle.

4. **Use Laravel Queues**: For better performance and scalability, consider using Laravel queues to handle webhook jobs asynchronously. This ensures that your application remains responsive even during heavy webhook traffic.

Now, your Laravel application is ready to receive and process Squareup webhook events in real-time.

## Support

If you have any questions or need assistance, feel free to [contact us](mailto:mahan.shoghy7@gmail.com).
if there are specific code examples you'd like to include in the README to help users get started quickly, you can add those as well.

## Donate

If you find this package helpful and would like to support its development, you can make a donation in cryptocurrency. Your contributions will help maintain and improve this package.

You can send your contribution to the following address in your MetaMask wallet: <br>
Wallet Address:
```
0x4B3d57AC72B57a76b778Bac4e3b32d2d4d729955
```

Thank you for your support!
