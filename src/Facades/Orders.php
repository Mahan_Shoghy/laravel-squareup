<?php

namespace MahanShoghy\LaravelSquareup\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Order;

/**
 * @method static static setToken(string $token)
 * @method static null|string getToken()
 * @method static Order create(array $order, ?string $idempotency_key = null)
 * @method static Collection batchRetrieve(array $order_ids, ?string $location_id = null)
 * @method static Order calculate(array $order, ?array $proposed_rewards = null)
 * @method static Order clone(string $order_id, ?int $version = null, ?string $idempotency_key = null)
 * @method static Collection search(array $location_ids = [], ?string $cursor = null, ?array $query = [], int $limit = 500, bool $return_entries = false)
 * @method static Order retrieve(string $order_id)
 * @method static Order update(string $order_id, array $order, array $fields_to_clear = [], ?string $idempotency_key = null)
 * @method static Order pay(string $order_id, string $idempotency_key, ?int $order_version = null, array $payment_ids = [])
 */
class Orders extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MahanShoghy\LaravelSquareup\App\Commerce\Orders\Orders::class;
    }
}
