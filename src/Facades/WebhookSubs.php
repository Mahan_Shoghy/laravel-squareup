<?php

namespace MahanShoghy\LaravelSquareup\Facades;

use Illuminate\Support\Facades\Facade;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\EventType;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Pagination;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Subscription;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\TestSubscription;

/**
 * @method static EventType listEvents(?string $api_version = null)
 * @method static Pagination list(?string $cursor = null, ?bool $include_disabled = null, ?string $sort_order = null, int $limit = 100)
 * @method static Subscription retrieve(string $subscription_id)
 * @method static Subscription create(array $data)
 * @method static Subscription update(string $subscription_id, array $data)
 * @method static string updateSignature(string $subscription_id, string $idempotency_key)
 * @method static void delete(string $subscription_id)
 * @method static TestSubscription test(string $subscription_id, string $event_type)
 */
class WebhookSubs extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\WebhookSubs::class;
    }
}
