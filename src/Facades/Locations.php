<?php

namespace MahanShoghy\LaravelSquareup\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Location;

/**
 * @method static static setToken(string $token)
 * @method static null|string getToken()
 * @method static Collection list()
 * @method static Location retrieve(string $location_id)
 * @method static Location create(array $location)
 * @method static Location update(string $location_id, array $location)
 */
class Locations extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MahanShoghy\LaravelSquareup\App\Merchants\Locations\Locations::class;
    }
}
