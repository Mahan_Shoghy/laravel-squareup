<?php

namespace MahanShoghy\LaravelSquareup\Facades;

use Illuminate\Support\Facades\Facade;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Token;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\TokenStatus;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Enums\OAuthGrantTypeEnum;

/**
 * @method static string authorize(?string $code_challenge = null)
 * @method static bool revokeToken(?string $access_token = null, ?string $merchant_id = null, bool $revoke_only_access_token = false)
 * @method static Token obtainToken(string $code, OAuthGrantTypeEnum $grant_type = OAuthGrantTypeEnum::AUTHORIZATION_CODE, ?string $state = null, ?string $code_verifier = null, bool $short_lived = false)
 * @method static TokenStatus retrieveTokenStatus(string $access_token)
 */
class OAuth extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \MahanShoghy\LaravelSquareup\App\Essentials\OAuth\OAuth::class;
    }
}
