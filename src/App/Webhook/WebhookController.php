<?php

namespace MahanShoghy\LaravelSquareup\App\Webhook;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use MahanShoghy\LaravelSquareup\App\Webhook\Jobs\ProcessSquareupWebhookJob;

class WebhookController
{
    public function __invoke(Request $request): JsonResponse
    {
        dispatch(new ProcessSquareupWebhookJob($request->all()));

        return response()->json();
    }
}
