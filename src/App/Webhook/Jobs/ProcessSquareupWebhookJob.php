<?php

namespace MahanShoghy\LaravelSquareup\App\Webhook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use MahanShoghy\LaravelDoordash\App\Webhook\Model\Payload;

class ProcessSquareupWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public array $payload){}

    public function handle(): void
    {
        $event = str($this->payload['type'])->replace('.', '_')->value();
        $job_class = $this->determineJobClass($event);

        if ($job_class){
            dispatch(new $job_class($this->payload));
        }
    }

    private function determineJobClass(string $event_name): ?string
    {
        return config("squareup.webhook.jobs.{$event_name}", null);
    }
}
