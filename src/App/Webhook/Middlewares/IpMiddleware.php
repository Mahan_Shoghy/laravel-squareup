<?php

namespace MahanShoghy\LaravelSquareup\App\Webhook\Middlewares;

use Closure;
use Illuminate\Http\Request;

class IpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ips = (array) config('squareup.webhook.ips');

        if ($ips && !in_array($request->ip(), $ips)){
            abort(403, 'Your ip address is not valid for this type of request');
        }

        return $next($request);
    }
}
