<?php

namespace MahanShoghy\LaravelSquareup\App;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupException;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupRequestException;

class ApiProvider
{
    protected string $base_url;

    private ?string $token = null;

    protected PendingRequest $http;

    public function __construct()
    {
        $this->base_url = config('squareup.hostname');

        $this->http = Http::acceptJson();
    }

    public function setToken(string $token): static
    {
        $this->token = $token;
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @throws SquareupException()
     */
    protected function tokenMustExists(string $method): void
    {
        if (! $this->getToken()){
            throw new SquareupException("Token must set for $method(). use setToken(".'$token'.") before calling $method()");
        }
    }

    /**
     * @throws SquareupRequestException
     */
    protected function checkResponse(Response $response): void
    {
        if ($response->failed()){
            throw new SquareupRequestException($response);
        }
    }
}
