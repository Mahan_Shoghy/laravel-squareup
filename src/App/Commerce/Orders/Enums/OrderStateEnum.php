<?php

namespace MahanShoghy\LaravelSquareup\App\Commerce\Orders\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum OrderStateEnum: string
{
    use EnumHelper;

    case OPEN = 'OPEN';
    case COMPLETED = 'COMPLETED';
    case CANCELED = 'CANCELED';
    case DRAFT = 'DRAFT';

    public function title(): string
    {
        return str($this->name)->title()->value();
    }
}
