<?php

namespace MahanShoghy\LaravelSquareup\App\Commerce\Orders\Interfaces;

use Illuminate\Support\Collection;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Order;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupException;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupRequestException;

interface OrdersInterface
{

    /**
     * Creates a new order that can include information about products for purchase and settings to apply to the purchase.
     *
     * @param array $order The order to create. If this field is set, the only other top-level field that can be set is the idempotency_key.
     * @param string|null $idempotency_key A value you specify that uniquely identifies this order among orders you have created. If you are unsure whether a particular order was created successfully, you can try it again with the same idempotency key without worrying about creating duplicate orders. For more information, see https://developer.squareup.com/docs/build-basics/common-api-patterns/idempotency.
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function create(array $order, ?string $idempotency_key = null): Order;

    /**
     * Retrieves a set of orders by their IDs.
     * If a given order ID does not exist, the ID is ignored instead of generating an error.
     *
     * @param string[] $order_ids The IDs of the orders to retrieve. A maximum of 100 orders can be retrieved per request.
     * @param string|null $location_id The ID of the location for these orders. This field is optional: omit it to retrieve orders within the scope of the current authorization's merchant ID.
     * @return Collection
     * @throws SquareupException|SquareupRequestException
     */
    public function batchRetrieve(array $order_ids, ?string $location_id = null): Collection;

    /**
     * Enables applications to preview order pricing without creating an order.
     *
     * @param array $order
     * @param array|null $proposed_rewards
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function calculate(array $order, ?array $proposed_rewards = null): Order;

    /**
     * Creates a new order, in the DRAFT state, by duplicating an existing order.
     * The newly created order has only the core fields (such as line items, taxes, and discounts) copied from the original order.
     *
     * @param string $order_id The ID of the order to clone.
     * @param int|null $version An optional order version for concurrency protection. If a version is provided, it must match the latest stored version of the order to clone. If a version is not provided, the API clones the latest version.
     * @param string|null $idempotency_key A value you specify that uniquely identifies this clone request. If you are unsure whether a particular order was cloned successfully, you can reattempt the call with the same idempotency key without worrying about creating duplicate cloned orders. The originally cloned order is returned. For more information, see https://developer.squareup.com/docs/build-basics/common-api-patterns/idempotency.
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function clone(string $order_id, ?int $version = null, ?string $idempotency_key = null): Order;

    /**
     * Search all orders for one or more locations. Orders include all sales, returns,
     * and exchanges regardless of how or when they entered the Square ecosystem
     * (such as Point of Sale, Invoices, and Connect APIs).
     *
     * @param string[] $location_ids The location IDs for the orders to query. All locations must belong to the same merchant.
     * @param string|null $cursor A pagination cursor returned by a previous call to this endpoint. Provide this cursor to retrieve the next set of results for your original query. For more information, see https://developer.squareup.com/docs/build-basics/common-api-patterns/pagination.
     * @param array|null $query Query conditions used to filter or sort the results. Note that when retrieving additional pages using a cursor, you must use the original query.
     * @param int $limit The maximum number of results to be returned in a single page. It is possible to receive fewer results than the specified limit on a given page.
     * @param bool $return_entries A Boolean that controls the format of the search results. If true, SearchOrders returns OrderEntry objects. If false, SearchOrders returns complete order objects.
     * @return Collection
     * @throws SquareupException|SquareupRequestException
     */
    public function search(array $location_ids = [], ?string $cursor = null, ?array $query = [], int $limit = 500, bool $return_entries = false): Collection;

    /**
     * Retrieves an Order by ID.
     *
     * @param string $order_id The ID of the order to retrieve.
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function retrieve(string $order_id): Order;

    /**
     * Updates an open order by adding, replacing, or deleting fields.
     * Orders with a COMPLETED or CANCELED state cannot be updated.
     *
     * @param string $order_id The ID of the order to update.
     * @param array $order The sparse order containing only the fields to update and the version to which the update is being applied.
     * @param string[] $fields_to_clear The dot notation paths fields to clear. For example, line_items[uid].note. For more information, see Deleting fields.
     * @param string|null $idempotency_key A value you specify that uniquely identifies this update request. If you are unsure whether a particular update was applied to an order successfully, you can reattempt it with the same idempotency key without worrying about creating duplicate updates to the order. The latest order version is returned. For more information, see https://developer.squareup.com/docs/build-basics/common-api-patterns/idempotency.
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function update(string $order_id, array $order, array $fields_to_clear = [], ?string $idempotency_key = null): Order;

    /**
     * Pay for an order using one or more approved payments or settle an order with a total of 0. The total of the payment_ids listed in the request must be equal to the order total. Orders with a total amount of 0 can be marked as paid by specifying an empty array of payment_ids in the request.
     *
     * @param string $order_id The ID of the order being paid.
     * @param string $idempotency_key A value you specify that uniquely identifies this request among requests you have sent. If you are unsure whether a particular payment request was completed successfully, you can reattempt it with the same idempotency key without worrying about duplicate payments. For more information, see Idempotency.
     * @param int|null $order_version The version of the order being paid. If not supplied, the latest version will be paid.
     * @param array $payment_ids The IDs of the payments to collect. The payment total must match the order total.
     * @return Order
     * @throws SquareupException|SquareupRequestException
     */
    public function pay(string $order_id, string $idempotency_key, ?int $order_version = null, array $payment_ids = []): Order;
}
