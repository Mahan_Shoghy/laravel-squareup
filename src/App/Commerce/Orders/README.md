# Squareup Orders API

The Squareup Orders API provides comprehensive functionalities for managing sales data, processing payments, and handling orders within your application. You can itemize payments using custom line items or catalog objects, send orders to physical Point of Sale devices for fulfillment, attach customers to payments, and access detailed itemization data, customer references, and other relevant details from sales made using POS or online. For more detailed information, refer to the [Squareup API Documentation](https://developer.squareup.com/reference/square_2023-10-20/orders-api).

## Usage

Utilize the following methods provided by the `Orders` facade to interact with the Squareup Orders API:

```php
use MahanShoghy\LaravelSquareup\Facades\Orders;

// Set the access token for authentication
Orders::setToken($token): static;

// Retrieve the currently set access token
Orders::getToken(): ?string;
```

Before using any other method, ensure that you set the access token using the `setToken` method:

```php
// Create a new order
Orders::create($order, $idempotency_key): Order;

// Retrieve multiple orders in a batch
Orders::batchRetrieve($order_ids, $location_id): Collection;

// Calculate an order based on proposed rewards
Orders::calculate($order, $proposed_rewards): Order;

// Clone an existing order
Orders::clone($order_id, $version , $idempotency_key): Order;

// Search for orders based on specific criteria
Orders::search($location_ids, $cursor, $query, $limit, $return_entries): Collection;

// Retrieve a specific order by its ID
Orders::retrieve($order_id): Order;

// Update an existing order with specified details
Orders::update($order_id, $order, $fields_to_clear, $idempotency_key): Order;

// Process a payment for a specific order
Orders::pay($order_id, $idempotency_key, $order_version, $payment_ids = []): Order;
```
