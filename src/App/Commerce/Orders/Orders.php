<?php

namespace MahanShoghy\LaravelSquareup\App\Commerce\Orders;

use Illuminate\Support\Collection;
use MahanShoghy\LaravelSquareup\App\ApiProvider;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Order;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Parser\OrderParser;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Interfaces\OrdersInterface;

class Orders extends ApiProvider implements OrdersInterface
{
    public function create(array $order, ?string $idempotency_key = null): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders", compact(['order', 'idempotency_key']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new OrderParser($response['order']))->get();
    }

    public function batchRetrieve(array $order_ids, ?string $location_id = null): Collection
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders/batch-retrieve", compact(['order_ids', 'location_id']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return collect($response['orders'])->map(function ($item){
            return (new OrderParser($item));
        });
    }

    public function calculate(array $order, ?array $proposed_rewards = null): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders/calculate", compact(['order', 'proposed_rewards']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new OrderParser($response['order']))->get();
    }

    public function clone(string $order_id, ?int $version = null, ?string $idempotency_key = null): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders/clone", compact(['order_id', 'version', 'idempotency_key']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new OrderParser($response['order']))->get();
    }

    public function search(array $location_ids = [], ?string $cursor = null, ?array $query = [], int $limit = 500, bool $return_entries = false): Collection
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders/search", compact([
                'location_ids',
                'cursor',
                'query',
                'limit',
                'return_entries'
            ]));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        $data = collect($response);
        $data->orders = $data->orders?->map(function ($order) {
            return (new OrderParser($order))->get();
        });

        return $data;
    }

    public function retrieve(string $order_id): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->get("$this->base_url/v2/orders/$order_id");

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new OrderParser($response['order']))->get();
    }

    public function update(string $order_id, array $order, array $fields_to_clear = [], ?string $idempotency_key = null): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->put("$this->base_url/v2/orders/$order_id", compact(['order', 'fields_to_clear', 'idempotency_key']));

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new OrderParser($response['order']))->get();
    }

    public function pay(string $order_id, string $idempotency_key, ?int $order_version = null, array $payment_ids = []): Order
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/orders/$order_id/pay", compact([
                'idempotency_key',
                'order_version',
                'payment_ids'
            ]));

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new OrderParser($response['order']))->get();
    }
}
