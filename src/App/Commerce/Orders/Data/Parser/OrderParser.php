<?php

namespace MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data\Order;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Enums\OrderStateEnum;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class OrderParser implements ParserInterface
{
    protected Order $order;

    public function __construct(array $data)
    {
        $created_at = (empty($data['created_at'])) ? null
            : Carbon::parse($data['created_at']);

        $updated_at = (empty($data['updated_at'])) ? null
            : Carbon::parse($data['updated_at']);

        $closed_at = (empty($data['closed_at'])) ? null
            : Carbon::parse($data['closed_at']);

        $state = (empty($data['$state'])) ? null
            : OrderStateEnum::getFrom($data['state']);

        $this->order = new Order(
            $data['id'] ?? null,
            $data['location_id'] ?? null,
            $data['line_items'] ?? null,
            $created_at,
            $updated_at,
            $data['version'] ?? null,
            $data['total_tax_money'] ?? null,
            $data['total_discount_money'] ?? null,
            $data['total_money'] ?? null,
            $closed_at,
            $data['tenders'] ?? null,
            $data['total_service_charge_money'] ?? null,
            $data['net_amounts'] ?? null,
            $data['source'] ?? null,
            $state,
        );
    }

    public function get(): Order
    {
        return $this->order;
    }
}
