<?php

namespace MahanShoghy\LaravelSquareup\App\Commerce\Orders\Data;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Commerce\Orders\Enums\OrderStateEnum;
use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Order implements DataInterface
{
    public function __construct(
        public readonly ?string         $id,
        public readonly ?string         $location_id,
        public readonly ?array          $line_items,
        public readonly ?Carbon         $created_at,
        public readonly ?Carbon         $updated_at,
        public readonly ?string         $version,
        public readonly ?array          $total_tax_money,
        public readonly ?array          $total_discount_money,
        public readonly ?array          $total_money,
        public readonly ?Carbon         $closed_at,
        public readonly ?array          $tenders,
        public readonly ?array          $total_service_charge_money,
        public readonly ?array          $net_amounts,
        public readonly ?array          $source,
        public readonly ?OrderStateEnum $state,
    ){}

    public static function fake(): static
    {
        return new static(
            "lgwOlEityYPJtcuvKTVKT1pA986YY",
            "P3CCK6HSNDAS7",
            [
                [
                    "uid" => "QW6kofLHJK7JEKMjlSVP5C",
                    "quantity" => "1",
                    "name" => "Item 1",
                    "base_price_money" => [
                        "amount" => 500,
                        "currency" => "USD"
                    ],
                    "gross_sales_money" => [
                        "amount" => 500,
                        "currency" => "USD"
                    ],
                    "total_tax_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_service_charge_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_discount_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_money" => [
                        "amount" => 500,
                        "currency" => "USD"
                    ]
                ],
                [
                    "uid" => "zhw8MNfRGdFQMI2WE1UBJD",
                    "quantity" => "2",
                    "name" => "Item 2",
                    "base_price_money" => [
                        "amount" => 750,
                        "currency" => "USD"
                    ],
                    "gross_sales_money" => [
                        "amount" => 1500,
                        "currency" => "USD"
                    ],
                    "total_tax_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_service_charge_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_discount_money" => [
                        "amount" => 0,
                        "currency" => "USD"
                    ],
                    "total_money" => [
                        "amount" => 1500,
                        "currency" => "USD"
                    ]
                ]
            ],
            Carbon::parse("2019-08-06T02:47:35.693Z"),
            Carbon::parse("2019-08-06T02:47:37.140Z"),
            4,
            [
                "amount" => 0,
                "currency" => "USD"
            ],
            [
                "amount" => 0,
                "currency" => "USD"
            ],
            [
                "amount" => 2000,
                "currency" => "USD"
            ],
            Carbon::parse("2019-08-06T02:47:37.140Z"),
            [
                [
                    "id" => "EnZdNAlWCmfh6Mt5FMNST1o7taB",
                    "location_id" => "P3CCK6HSNDAS7",
                    "transaction_id" => "lgwOlEityYPJtcuvKTVKT1pA986YY",
                    "created_at" => "2019-08-06T02 =>47 =>36.293Z",
                    "amount_money" => [
                        "amount" => 1000,
                        "currency" => "USD"
                    ],
                    "type" => "CARD",
                    "card_details" => [
                        "status" => "CAPTURED",
                        "card" => [
                            "card_brand" => "VISA",
                            "last_4" => "1111",
                            "exp_month" => 2,
                            "exp_year" => 2022,
                            "fingerprint" => "sq-1-n_BL15KP87ClDa4-h2nXOI0fp5VnxNH6hfhzqhptTfAgxgLuGFcg6jIPngDz4IkkTQ"
                        ],
                        "entry_method" => "KEYED"
                    ],
                    "payment_id" => "EnZdNAlWCmfh6Mt5FMNST1o7taB"
                ],
                [
                    "id" => "0LRiVlbXVwe8ozu4KbZxd12mvaB",
                    "location_id" => "P3CCK6HSNDAS7",
                    "transaction_id" => "lgwOlEityYPJtcuvKTVKT1pA986YY",
                    "created_at" => "2019-08-06T02 =>47 =>36.809Z",
                    "amount_money" => [
                        "amount" => 1000,
                        "currency" => "USD"
                    ],
                    "type" => "CARD",
                    "card_details" => [
                        "status" => "CAPTURED",
                        "card" => [
                            "card_brand" => "VISA",
                            "last_4" => "1111",
                            "exp_month" => 2,
                            "exp_year" => 2022,
                            "fingerprint" => "sq-1-n_BL15KP87ClDa4-h2nXOI0fp5VnxNH6hfhzqhptTfAgxgLuGFcg6jIPngDz4IkkTQ"
                        ],
                        "entry_method" => "KEYED"
                    ],
                    "payment_id" => "0LRiVlbXVwe8ozu4KbZxd12mvaB"
                ]
            ],
            [
                "amount" => 0,
                "currency" => "USD"
            ],
            [
                "total_money" => [
                    "amount" => 2000,
                    "currency" => "USD"
                ],
                "tax_money" => [
                    "amount" => 0,
                    "currency" => "USD"
                ],
                "discount_money" => [
                    "amount" => 0,
                    "currency" => "USD"
                ],
                "tip_money" => [
                    "amount" => 0,
                    "currency" => "USD"
                ],
                "service_charge_money" => [
                    "amount" => 0,
                    "currency" => "USD"
                ]
            ],
            [
                "name" => "Source Name"
            ],
            OrderStateEnum::COMPLETED
        );
    }
}
