<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations;

use Illuminate\Support\Collection;
use MahanShoghy\LaravelSquareup\App\ApiProvider;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Location;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Parser\LocationParser;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Interfaces\LocationInterface;
use MahanShoghy\LaravelSquareup\Interfaces\ApiProviderInterface;

class Locations extends ApiProvider implements LocationInterface, ApiProviderInterface
{
    public function list(): Collection
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->get("$this->base_url/v2/locations");

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return collect($response['locations'])->map(function ($item){
            return (new LocationParser($item))->get();
        });
    }

    public function retrieve(string $location_id): Location
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->get("$this->base_url/v2/locations/$location_id");

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new LocationParser($response['location']))->get();
    }

    public function create(array $location): Location
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->post("$this->base_url/v2/locations", compact('location'));

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new LocationParser($response['location']))->get();
    }

    public function update(string $location_id, array $location): Location
    {
        $this->tokenMustExists(__FUNCTION__);

        $response = $this->http->withToken($this->getToken())
            ->put("$this->base_url/v2/locations/$location_id", compact('location'));

        $this->checkResponse($response);
        $response = json_decode($response, true);

        return (new LocationParser($response['location']))->get();
    }
}
