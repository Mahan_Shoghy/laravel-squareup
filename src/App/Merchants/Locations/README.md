# Squareup Locations API

The Squareup Locations API allows for the creation and management of a seller's business locations. With this API, you can create and manage data related to various business locations, including addresses, names, and business hours. For more detailed information, refer to the [Squareup Documentation](https://developer.squareup.com/reference/square_2023-10-20/locations-api).

## Usage

```php
use MahanShoghy\LaravelSquareup\Facades\Locations;

// Set the access token for authentication
Orders::setToken($token): static;

// Retrieve the currently set access token
Orders::getToken(): ?string;
```

Before using any other method, ensure that you set the access token using the `setToken` method:

```php
// Retrieve a list of all locations
Locations::list(): Collection;

// Retrieve details for a specific location by its ID
Locations::retrieve($location_id): Location;

// Create a new location
Locations::create($location): Location;

// Update an existing location by its ID
Locations::update($location_id, $location): Location;
```

These methods enable you to effectively manage your business's locations, providing the necessary functionality to create, retrieve, and update location-related data within the Squareup Locations API.
