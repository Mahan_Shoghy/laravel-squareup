<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data;

use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Coordinates implements DataInterface
{
    /**
     * @param float|null $latitude The latitude of the coordinate expressed in degrees.
     * @param float|null $longitude The longitude of the coordinate expressed in degrees.
     */
    public function __construct(
        public readonly ?float $latitude,
        public readonly ?float $longitude,
    ){}

    public static function fake(): static
    {
        return new static(
            39.08059,
            -81.98750
        );
    }
}
