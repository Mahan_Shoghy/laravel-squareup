<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Parser\AddressParser;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums\LocationStatusEnum;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums\LocationTypeEnum;
use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Location implements DataInterface
{
    /**
     * @param string|null $id Read only A short generated string of letters and numbers that uniquely identifies this location instance.
     * @param string|null $name The name of the location. This information appears in the Seller Dashboard as the nickname. A location name must be unique within a seller account.
     * @param Address $address The physical address of the location.
     * @param string|null $timezone The IANA time zone identifier for the time zone of the location. For example, America/Los_Angeles.
     * @param array $capabilities Read only The Square features that are enabled for the location.
     * @param LocationStatusEnum $status The status of the location.
     * @param Carbon|null $created_at Read only The time when the location was created
     * @param string|null $merchant_id Read only The ID of the merchant that owns the location.
     * @param string|null $country Read only The country of the location, in the two-letter format of ISO 3166. For example, US or JP.
     * @param string|null $language_code The language associated with the location, in BCP 47 format. For more information, see https://developer.squareup.com/docs/build-basics/general-considerations/language-preferences
     * @param string|null $currency Read only The currency used for all transactions at this location, in ISO 4217 format. For example, the currency code for US dollars is USD.
     * @param string|null $phone_number The phone number of the location. For example, +1 855-700-6000.
     * @param string|null $business_name The name of the location's overall business. This name is present on receipts and other customer-facing branding, and can be changed no more than three times in a twelve-month period.
     * @param LocationTypeEnum|null $type The type of the location.
     * @param string|null $website_url The website URL of the location. For example, https://squareup.com.
     * @param array|null $business_hours The hours of operation for the location.
     * @param string|null $business_email The email address of the location. This can be unique to the location and is not always the email address for the business owner or administrator.
     * @param string|null $description The description of the location. For example, Main Street location
     * @param string|null $twitter_username The Twitter username of the location without the '@' symbol. For example, Square.
     * @param string|null $instagram_username The Instagram username of the location without the '@' symbol. For example, square.
     * @param string|null $facebook_url The Facebook profile URL of the location. The URL should begin with 'facebook.com/'. For example, https://www.facebook.com/square.
     * @param Coordinates|null $coordinates The physical coordinates (latitude and longitude) of the location.
     * @param string|null $logo_url Read only The URL of the logo image for the location. When configured in the Seller Dashboard (Receipts section), the logo appears on transactions (such as receipts and invoices) that Square generates on behalf of the seller. This image should have a roughly square (1:1) aspect ratio and should be at least 200x200 pixels.
     * @param string|null $pos_background_url Read only The URL of the Point of Sale background image for the location.
     * @param string|null $mcc A four-digit number that describes the kind of goods or services sold at the location. The merchant category code (MCC) of the location as standardized by ISO 18245. For example, 5045, for a location that sells computer goods and software
     * @param string|null $full_format_logo_url Read only The URL of a full-format logo image for the location. When configured in the Seller Dashboard (Receipts section), the logo appears on transactions (such as receipts and invoices) that Square generates on behalf of the seller. This image can be wider than it is tall and should be at least 1280x648 pixels.
     * @param TaxIds|null $tax_ids Read only The tax IDs for this location.
     */
    public function __construct(
        public readonly ?string            $id,
        public readonly ?string            $name,
        Address                            $address,
        public readonly ?string            $timezone,
        array                              $capabilities,
        public readonly LocationStatusEnum $status,
        public readonly ?Carbon            $created_at,
        public readonly ?string            $merchant_id,
        public readonly ?string            $country,
        public readonly ?string            $language_code,
        public readonly ?string            $currency,
        public readonly ?string            $phone_number,
        public readonly ?string            $business_name,
        public readonly ?LocationTypeEnum  $type,
        public readonly ?string            $website_url,
        public readonly ?array             $business_hours,
        public readonly ?string            $business_email,
        public readonly ?string            $description,
        public readonly ?string            $twitter_username,
        public readonly ?string            $instagram_username,
        public readonly ?string            $facebook_url,
        public readonly ?Coordinates       $coordinates,
        public readonly ?string            $logo_url,
        public readonly ?string            $pos_background_url,
        public readonly ?string            $mcc,
        public readonly ?string            $full_format_logo_url,
        public readonly ?TaxIds            $tax_ids,
    ){}

    public static function fake(): static
    {
        return new static(
            "18YC4JDH91E1H",
            "Grant Park",
            (new AddressParser([
                "address_line_1" => "123 Main St",
                "locality" => "San Francisco",
                "administrative_district_level_1" => "CA",
                "postal_code" => "94114",
                "country" => "US"
            ]))->get(),
            "America/Los_Angeles",
            [
                "CREDIT_CARD_PROCESSING"
            ],
            LocationStatusEnum::ACTIVE,
            Carbon::parse("2016-09-19T17:33:12Z"),
            "3MYCJG5GVYQ8Q",
            "US",
            "en-US",
            "USD",
            "+1 650-354-7217",
            "Jet Fuel Coffee",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        );
    }
}
