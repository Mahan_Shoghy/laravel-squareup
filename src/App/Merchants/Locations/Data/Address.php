<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data;

use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Address implements DataInterface
{
    /**
     * @param string|null $address_line_1 The first line of the address. Fields that start with address_line provide the address's most specific details, like street number, street name, and building name. They do not provide less specific details like city, state/province, or country (these details are provided in other fields).
     * @param string|null $address_line_2 The second line of the address, if any.
     * @param string|null $address_line_3 The third line of the address, if any.
     * @param string|null $locality The city or town of the address. For a full list of field meanings by country, see https://developer.squareup.com/docs/build-basics/working-with-addresses
     * @param string|null $sublocality A civil region within the address's locality, if any.
     * @param string|null $sublocality_2 A civil region within the address's sublocality, if any.
     * @param string|null $sublocality_3 A civil region within the address's sublocality_2, if any.
     * @param string|null $administrative_district_level_1 A civil entity within the address's country. In the US, this is the state. For a full list of field meanings by country, see https://developer.squareup.com/docs/build-basics/working-with-addresses
     * @param string|null $administrative_district_level_2 A civil entity within the address's administrative_district_level_1. In the US, this is the county.
     * @param string|null $administrative_district_level_3 A civil entity within the address's administrative_district_level_2, if any.
     * @param string|null $postal_code The address's postal code. For a full list of field meanings by country, see The address's postal code. For a full list of field meanings by country, se
     * @param string|null $country The address's country, in the two-letter format of ISO 3166. For example, US or FR.
     * @param string|null $first_name Optional first name when it's representing recipient.
     * @param string|null $last_name Optional last name when it's representing recipient.
     */
    public function __construct(
        public readonly ?string $address_line_1,
        public readonly ?string $address_line_2,
        public readonly ?string $address_line_3,
        public readonly ?string $locality,
        public readonly ?string $sublocality,
        public readonly ?string $sublocality_2,
        public readonly ?string $sublocality_3,
        public readonly ?string $administrative_district_level_1,
        public readonly ?string $administrative_district_level_2,
        public readonly ?string $administrative_district_level_3,
        public readonly ?string $postal_code,
        public readonly ?string $country,
        public readonly ?string $first_name,
        public readonly ?string $last_name,
    ){}

    public static function fake(): static
    {
        return new static(
            "123 Main St",
      null,
      null,
      "San Francisco",
      null,
      null,
      null,
      "CA",
      null,
      null,
            "94114",
            "US",
            null,
            null
        );
    }
}
