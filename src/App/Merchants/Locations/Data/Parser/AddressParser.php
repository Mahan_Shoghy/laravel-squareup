<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Parser;

use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Address;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class AddressParser implements ParserInterface
{
    private Address $address;

    public function __construct(array $data)
    {
        $this->address = new Address(
            $data['address_line_1'] ?? null,
            $data['address_line_2'] ?? null,
            $data['address_line_3'] ?? null,
            $data['locality'] ?? null,
            $data['sublocality'] ?? null,
            $data['sublocality_2'] ?? null,
            $data['sublocality_3'] ?? null,
            $data['administrative_district_level_1'] ?? null,
            $data['administrative_district_level_2'] ?? null,
            $data['administrative_district_level_3'] ?? null,
            $data['postal_code'] ?? null,
            $data['country'] ?? null,
            $data['first_name'] ?? null,
            $data['last_name'] ?? null,
        );
    }

    public function get(): Address
    {
        return $this->address;
    }
}
