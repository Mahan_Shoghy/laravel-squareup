<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Parser;

use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\TaxIds;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class TaxIdsParser implements ParserInterface
{
    private TaxIds $tax_ids;

    public function __construct(array $data)
    {
        $this->tax_ids = new TaxIds(
            $data['eu_vat'] ?? null,
            $data['fr_siret'] ?? null,
            $data['fr_naf'] ?? null,
            $data['es_nif'] ?? null,
            $data['jp_qii'] ?? null
        );
    }

    public function get(): TaxIds
    {
        return $this->tax_ids;
    }
}
