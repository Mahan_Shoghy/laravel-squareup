<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Coordinates;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Location;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums\LocationStatusEnum;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums\LocationTypeEnum;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class LocationParser implements ParserInterface
{
    private Location $location;

    public function __construct(array $data)
    {
        $address = (empty($data['address'])) ? null
            : (new AddressParser($data['address']))->get();

        $status = (empty($data['status'])) ? null
            : LocationStatusEnum::getFrom($data['status']);

        $created_at = (empty($data['created_at'])) ? null
            : Carbon::parse($data['created_at']);

        $type = (empty($data['type'])) ? null
            : LocationTypeEnum::getFrom($data['type']);

        $coordinates = (empty($data['coordinates'])) ? null
            : new Coordinates($data['coordinates']['latitude'], $data['coordinates']['longitude']);

        $tax_ids = (empty($data['tax_ids'])) ? null
            : (new TaxIdsParser($data['tax_ids']))->get();

        $this->location = new Location(
            $data['id'] ?? null,
            $data['name'] ?? null,
            $address,
            $data['timezone'] ?? null,
            $data['capabilities'] ?? null,
            $status,
            $created_at,
            $data['merchant_id'] ?? null,
            $data['country'] ?? null,
            $data['language_code'] ?? null,
            $data['currency'] ?? null,
            $data['phone_number'] ?? null,
            $data['business_name'] ?? null,
            $type,
            $data['website_url'] ?? null,
            $data['business_hours'] ?? null,
            $data['business_email'] ?? null,
            $data['description'] ?? null,
            $data['twitter_username'] ?? null,
            $data['instagram_username'] ?? null,
            $data['facebook_url'] ?? null,
            $coordinates,
            $data['logo_url'] ?? null,
            $data['pos_background_url'] ?? null,
            $data['mcc'] ?? null,
            $data['full_format_logo_url'] ?? null,
            $tax_ids
        );
    }

    public function get(): Location
    {
        return $this->location;
    }
}
