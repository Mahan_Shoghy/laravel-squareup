<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data;

use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class TaxIds implements DataInterface
{
    /**
     * @param string|null $eu_vat Read only The EU VAT number for this location. For example, IE3426675K. If the EU VAT number is present, it is well-formed and has been validated with VIES, the VAT Information Exchange System.
     * @param string|null $fr_siret Read only The SIRET (Système d'Identification du Répertoire des Entreprises et de leurs Etablissements) number is a 14-digit code issued by the French INSEE. For example, 39922799000021.
     * @param string|null $fr_naf Read only The French government uses the NAF (Nomenclature des Activités Françaises) to display and track economic statistical data. This is also called the APE (Activite Principale de l’Entreprise) code. For example, 6910Z.
     * @param string|null $es_nif Read only The NIF (Numero de Identificacion Fiscal) number is a nine-character tax identifier used in Spain. If it is present, it has been validated. For example, 73628495A.
     * @param string|null $jp_qii Read only The QII (Qualified Invoice Issuer) number is a 14-character tax identifier used in Japan. For example, T1234567890123.
     */
    public function __construct(
        public readonly ?string $eu_vat,
        public readonly ?string $fr_siret,
        public readonly ?string $fr_naf,
        public readonly ?string $es_nif,
        public readonly ?string $jp_qii
    ){}

    public static function fake(): static
    {
        return new static(
            'IE3426675K',
            '39922799000021',
            '6910Z',
            '73628495A',
            'T1234567890123'
        );
    }
}
