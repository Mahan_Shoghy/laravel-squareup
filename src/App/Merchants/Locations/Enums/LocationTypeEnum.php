<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum LocationTypeEnum: string
{
    use EnumHelper;

    case PHYSICAL = 'PHYSICAL';

    case MOBILE = 'MOBILE';

    public function title(): string
    {
        return str($this->name)->title()->value();
    }
}
