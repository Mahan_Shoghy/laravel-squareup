<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Enums;

use MahanShoghy\PhpEnumHelper\EnumHelper;

enum LocationStatusEnum: string
{
    use EnumHelper;

    case ACTIVE = 'ACTIVE';

    case INACTIVE = 'INACTIVE';

    public function title(): string
    {
        return str($this->name)->title()->value();
    }
}
