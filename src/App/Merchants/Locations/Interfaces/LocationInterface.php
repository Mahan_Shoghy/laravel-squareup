<?php

namespace MahanShoghy\LaravelSquareup\App\Merchants\Locations\Interfaces;

use Illuminate\Support\Collection;
use MahanShoghy\LaravelSquareup\App\Merchants\Locations\Data\Location;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupException;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupRequestException;

interface LocationInterface
{
    /**
     * Provides details about all the seller's locations, including those with an inactive status.
     * @return Collection
     * @throws SquareupException|SquareupRequestException
     */
    public function list(): Collection;

    /**
     * Retrieves details of a single location.
     * Specify "main" as the location ID to retrieve details of the main location.
     *
     * @param string $location_id
     * @return Location
     * @throws SquareupException|SquareupRequestException
     */
    public function retrieve(string $location_id): Location;

    /**
     * Creates a location. Creating new locations allows for separate configuration of receipt layouts, item prices,
     * and sales reports. Developers can use locations to separate sales activity through applications that integrate
     * with Square from sales activity elsewhere in a seller's account. Locations created programmatically with
     * the Locations API last forever and are visible to the seller for their own management.
     * Therefore, ensure that each location has a sensible and unique name.
     *
     * @param array $location
     * @return Location
     * @throws SquareupException|SquareupRequestException
     */
    public function create(array $location): Location;

    /**
     * Updates a location.
     *
     * @param string $location_id
     * @param array $location
     * @return Location
     * @throws SquareupException|SquareupRequestException
     */
    public function update(string $location_id, array $location): Location;
}
