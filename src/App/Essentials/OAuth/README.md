# Squareup OAuth Integration

Squareup OAuth integration allows your Laravel application to securely authenticate users and request specific permissions to access Squareup's powerful API features. Follow these steps to configure OAuth in your application.

For more detailed information, refer to the [Squareup API Documentation](https://developer.squareup.com/reference/square_2023-10-20/o-auth-api).

## Contents
- [Configuration](#configuration)
- [Usage](#usage)

## Configuration

To set up the Squareup OAuth configuration, make changes in the `squareup.php` configuration file:

```php
'oauth' => [
    /**
     * List of permissions that the application is requesting.
     * Include the necessary scopes for your application to access Squareup resources.
     * Refer to the Squareup documentation for available scopes.
     * More information: https://developer.squareup.com/reference/square_2023-10-20/o-auth-api/authorize#query__property-scope
     */
    'scopes' => [
        'ITEMS_WRITE',
        'CUSTOMERS_READ',
        // Add more scopes as required
    ],

    // The locale to present the permission request form in
    'locale' => 'en-US',

    // Define whether the user needs to log in to their Square account to view the Permission Request form
    'session' => true,

    // Use 'state' to protect against cross-site request forgery
    'state' => true,

    // The redirect URL assigned on the OAuth page for your application in the Developer Dashboard
    'redirect_uri' => env('APP_URL') . '/squareup/redirect_uri'
]
```

In this configuration, adjust the values according to your application's specific requirements.

## Usage

Utilize the following methods to interact with the Squareup OAuth integration:

```php
use MahanShoghy\LaravelSquareup\Facades\OAuth;


// Authorize the user and obtain the access token
OAuth::authorize($code_challenge): string;

// Revoke the access token for a specific user
OAuth::revokeToken($access_token, $merchant_id, $revoke_only_access_token): bool;

// Obtain a token for user authentication and authorization
OAuth::obtainToken($code, $grant_type, $state, $code_verifier, $short_lived): Token;

// Retrieve the status of a specific token
OAuth::retrieveTokenStatus($access_token): TokenStatus;
```

Ensure to integrate these methods with your application's authentication flow for seamless Squareup OAuth integration.
