<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth;

use MahanShoghy\LaravelSquareup\App\ApiProvider;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Parser\TokenParser;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Parser\TokenStatusParser;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Token;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\TokenStatus;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Enums\OAuthGrantTypeEnum;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Interfaces\OAuthInterface;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupException;

class OAuth extends ApiProvider implements OAuthInterface
{
    public function authorize(?string $code_challenge = null): string
    {
        $query = [
            'client_id' => config('squareup.id'),
            'scopes' => implode(' ', config('squareup.oauth.scopes')),
            'locale' => config('squareup.oauth.locale'),
            'session' => config('squareup.oauth.session'),
            'redirect_uri' => config('squareup.oauth.redirect_uri'),
        ];

        if ($code_challenge){
            $query['code_challenge'] = $code_challenge;
        }
        if (config('squareup.oauth.state')){
            $query['state'] = csrf_token();
        }

        $query = http_build_query($query);

        return "$this->base_url/oauth2/authorize?$query";
    }

    public function revokeToken(?string $access_token = null, ?string $merchant_id = null, bool $revoke_only_access_token = false): bool
    {
        $data = [
            'revoke_only_access_token' => $revoke_only_access_token,
            'client_id' => config('squareup.id')
        ];
        if ($access_token){
            $data['access_token'] = $access_token;
        }
        if ($merchant_id){
            $data['merchant_id'] = $merchant_id;
        }

        $secret = config('squareup.secret');
        $response = $this->http->withHeader('Authorization', "Client $secret")
            ->post("$this->base_url/oauth2/revoke", $data);

        $this->checkResponse($response);

        $response = json_decode($response->body(), true);
        return (bool) $response['success'];
    }

    public function obtainToken(string $code, OAuthGrantTypeEnum $grant_type = OAuthGrantTypeEnum::AUTHORIZATION_CODE, ?string $state = null, ?string $code_verifier = null, bool $short_lived = false): Token
    {
        if (config('squareup.oauth.state') && $state && $state !== csrf_token()){
            throw new SquareupException('The state parameter is not valid');
        }

        $data = [
            'client_id' => config('squareup.id'),
            'client_secret' => config('squareup.secret'),
            'redirect_uri' => config('squareup.oauth.redirect_uri'),
            'scopes' => config('squareup.oauth.scopes'),
            'grant_type' => $grant_type->value,
            $grant_type->codeName() => $code,
            'short_lived' => $short_lived,
        ];
        if ($code_verifier){
            $data['code_verifier'] = $code_verifier;
        }

        $response = $this->http->post("$this->base_url/oauth2/token", $data);

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new TokenParser($response))->get();
    }

    public function retrieveTokenStatus(string $access_token): TokenStatus
    {
        $response = $this->http->withToken($access_token)
            ->post("$this->base_url/oauth2/token/status");

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new TokenStatusParser($response))->get();
    }
}
