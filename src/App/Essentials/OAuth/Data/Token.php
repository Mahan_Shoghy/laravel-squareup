<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Token implements DataInterface
{
    /**
     * @param string|null $access_token A valid OAuth access token. OAuth access tokens are 64 bytes long. Provide the access token in a header with every request to Connect API endpoints. For more information, see https://developer.squareup.com/docs/oauth-api/walkthrough.
     * @param string|null $token_type This value is always bearer.
     * @param Carbon|null $expires_at The date when the access_token expires
     * @param string|null $merchant_id The ID of the authorizing merchant's business.
     * @param string|null $subscription_id LEGACY FIELD. The ID of a subscription plan the merchant signed up for. The ID is only present if the merchant signed up for a subscription plan during authorization.
     * @param string|null $plan_id LEGACY FIELD. The ID of the subscription plan the merchant signed up for. The ID is only present if the merchant signed up for a subscription plan during authorization.
     * @param string|null $id_token Deprecated! The OpenID token belonging to this person. This token is only present if the OPENID scope is included in the authorization request.
     * @param string|null $refresh_token A refresh token. OAuth refresh tokens are 64 bytes long. For more information, see https://developer.squareup.com/docs/oauth-api/refresh-revoke-limit-scope.
     * @param bool|null $short_lived A Boolean indicating that the access token is a short-lived access token. The short-lived access token returned in the response expires in 24 hours.
     * @param Carbon|null $refresh_token_expires_at The date when the refresh_token expires
     */
    public function __construct(
        public readonly ?string $access_token,
        public readonly ?string $token_type,
        public readonly ?Carbon $expires_at,
        public readonly ?string $merchant_id,
        public readonly ?string $subscription_id,
        public readonly ?string $plan_id,
        public readonly ?string $id_token,
        public readonly ?string $refresh_token,
        public readonly ?bool $short_lived,
        public readonly ?Carbon $refresh_token_expires_at
    ){}

    public static function fake(): static
    {
        return new static(
            Str::random(500),
            'bearer',
            now()->addHours(rand(1, 3)),
            Str::random(500),
            Str::random(500),
            Str::random(500),
            Str::random(500),
            Str::random(500),
            rand(0, 1),
            now()->addHours(rand(1, 3))
        );
    }
}
