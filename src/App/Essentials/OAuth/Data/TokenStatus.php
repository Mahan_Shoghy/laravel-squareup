<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class TokenStatus implements DataInterface
{
    public function __construct(
        public readonly ?array $scopes,
        public readonly ?Carbon $expires_at,
        public readonly ?string $client_id,
        public readonly ?string $merchant_id
    ){}

    public static function fake(): static
    {
        return new static(
            config('squareup.oauth.scopes'),
            now()->addHours(rand(1, 3)),
            Str::random(500),
            Str::random(500)
        );
    }
}
