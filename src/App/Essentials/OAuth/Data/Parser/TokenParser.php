<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Token;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class TokenParser implements ParserInterface
{
    private Token $token;

    public function __construct(array $data)
    {
        $expires_at = (empty($data['expires_at']))
            ? null
            : Carbon::parse($data['expires_at']);

        $refresh_token_expires_at = (empty($data['refresh_token_expires_at']))
            ? null
            : Carbon::parse($data['refresh_token_expires_at']);

        $this->token = new Token(
            $data['access_token'] ?? null,
            $data['token_type'] ?? null,
            $expires_at,
            $data['merchant_id'] ?? null,
            $data['subscription_id'] ?? null,
            $data['plan_id'] ?? null,
            $data['id_token'] ?? null,
            $data['refresh_token'] ?? null,
            $data['short_live'] ?? null,
            $refresh_token_expires_at
        );
    }

    public function get(): Token
    {
        return $this->token;
    }
}
