<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\TokenStatus;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class TokenStatusParser implements ParserInterface
{
    private TokenStatus $token_status;

    public function __construct(array $data)
    {
        $expires_at = (empty($data['expires_at']))
            ? null
            : Carbon::parse($data['expires_at']);

        $this->token_status = new TokenStatus(
            $data['token_status'] ?? null,
            $expires_at,
                $data['client_id'] ?? null,
                $data['merchant_id'] ?? null
        );
    }

    public function get(): TokenStatus
    {
        return $this->token_status;
    }
}
