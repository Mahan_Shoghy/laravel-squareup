<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Enums;

enum OAuthGrantTypeEnum: string
{
    case AUTHORIZATION_CODE = 'authorization_code';
    case REFRESH_TOKEN = 'refresh_token';
    case MIGRATION_TOKEN = 'migration_token';

    public function codeName(): string
    {
        return match ($this){
            self::AUTHORIZATION_CODE => 'code',
            self::REFRESH_TOKEN => 'refresh_token',
            self::MIGRATION_TOKEN => 'migration_token',
        };
    }
}
