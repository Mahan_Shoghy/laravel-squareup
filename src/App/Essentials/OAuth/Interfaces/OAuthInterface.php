<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Interfaces;

use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\Token;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Data\TokenStatus;
use MahanShoghy\LaravelSquareup\App\Essentials\OAuth\Enums\OAuthGrantTypeEnum;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupException;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupRequestException;

interface OAuthInterface
{
    /**
     * As part of a URL sent to a seller to authorize permissions for the developer, Authorize displays an authorization page and a list of requested permissions.
     * The completed URL looks similar to the following example: https://connect.squareup.com/oauth2/authorize?client_id={YOUR_APP_ID}&scope=CUSTOMERS_WRITE+CUSTOMERS_READ&session=False&state=82201dd8d83d23cc8a48caf52b
     * The seller can approve or deny the permissions. If approved,Authorize returns an AuthorizeResponse that is sent to the redirect URL and includes a state string and an authorization code. The code is used in the ObtainToken call to obtain an access token and a refresh token that the developer uses to manage resources on behalf of the seller.
     * Important: The AuthorizeResponse is sent to the redirect URL that you set on the OAuth page of your application in the Developer Dashboard.
     * If an error occurs or the seller denies the request, Authorize returns an error response that includes error and error_description values. If the error is due to the seller denying the request, the error value is access_denied and the error_description is user_denied.
     *
     * @param string|null $code_challenge When provided, the OAuth flow uses PKCE to authorize. The code_challenge will be associated with the authorization_code and a code_verifier will need to passed in to obtain the access token.
     * @return string
     */
    public function authorize(?string $code_challenge = null): string;

    /**
     * Revokes an access token generated with the OAuth flow.
     * If an account has more than one OAuth access token for your application, this endpoint revokes all of them, regardless of which token you specify.
     *
     * @param string|null $access_token The access token of the merchant whose token you want to revoke. Do not provide a value for merchant_id if you provide this parameter.
     * @param string|null $merchant_id The ID of the merchant whose token you want to revoke. Do not provide a value for access_token if you provide this parameter.
     * @param bool $revoke_only_access_token If true, terminate the given single access token, but do not terminate the entire authorization. Default: false
     * @return bool
     * @throws SquareupRequestException
     */
    public function revokeToken(?string $access_token = null, ?string $merchant_id = null, bool $revoke_only_access_token = false): bool;

    /**
     * Returns an OAuth access token and a refresh token unless the short_lived parameter is set to true, in which case the endpoint returns only an access token.
     *
     * @param string $code It will be the authorization code if grant_type is set to authorization_code, Or it will be a valid refresh token if grant_type is set to refresh_token, Or it will be a legacy OAuth access token obtained using a Connect API version prior to 2019-03-13. if grant_type is set to migration_token
     * @param OAuthGrantTypeEnum $grant_type Specifies the method to request an OAuth access token. Valid values are authorization_code, refresh_token, and migration_token
     * @param string|null $state Include state to help protect against cross-site request forgery
     * @param string|null $code_verifier Must be provided when using the PKCE OAuth flow if grant_type is set to authorization_code. The code_verifier is used to verify against the code_challenge associated with the authorization_code.
     * @param bool $short_lived A Boolean indicating a request for a short-lived access token. The short-lived access token returned in the response expires in 24 hours.
     * @return Token
     * @throws SquareupException|SquareupRequestException
     */
    public function obtainToken(
        string $code,
        OAuthGrantTypeEnum $grant_type = OAuthGrantTypeEnum::AUTHORIZATION_CODE,
        ?string $state = null,
        ?string $code_verifier = null,
        bool $short_lived = false
    ):
    Token;

    /**
     * Returns information about an OAuth access token or an application’s personal access token.
     *
     * @param string $access_token
     * @return TokenStatus
     * @throws SquareupRequestException
     */
    public function retrieveTokenStatus(string $access_token): TokenStatus;
}
