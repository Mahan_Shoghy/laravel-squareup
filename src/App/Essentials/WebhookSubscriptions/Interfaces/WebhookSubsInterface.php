<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Interfaces;

use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\EventType;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Pagination;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Subscription;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\TestSubscription;
use MahanShoghy\LaravelSquareup\Exceptions\SquareupRequestException;

interface WebhookSubsInterface
{
    /**
     * Lists all webhook event types that can be subscribed to.
     *
     * @param string|null $api_version The API version for which to list event types. Setting this field overrides the default version used by the application.
     * @return EventType
     * @throws SquareupRequestException
     */
    public function listEvents(?string $api_version = null): EventType;

    /**
     * Lists all webhook subscriptions owned by your application.
     *
     * @param string|null $cursor A pagination cursor returned by a previous call to this endpoint. Provide this to retrieve the next set of results for your original query.
     * @param bool|null $include_disabled Includes disabled Subscriptions. By default, all enabled Subscriptions are returned.
     * @param string|null $sort_order Sorts the returned list by when the Subscription was created with the specified order. This field defaults to ASC.
     * @param int $limit The maximum number of results to be returned in a single page. It is possible to receive fewer results than the specified limit on a given page. The default value of 100 is also the maximum allowed value.
     * @return Pagination
     * @throws SquareupRequestException
     */
    public function list(?string $cursor = null, ?bool $include_disabled = null, ?string $sort_order = null, int $limit = 100): Pagination;

    /**
     * Retrieves a webhook subscription identified by its ID.
     *
     * @param string $subscription_id The ID of the Subscription to retrieve.
     * @return Subscription
     * @throws SquareupRequestException
     */
    public function retrieve(string $subscription_id): Subscription;

    /**
     * Creates a webhook subscription.
     *
     * @param array $data For detail see https://developer.squareup.com/reference/square_2023-10-20/webhook-subscriptions-api/create-webhook-subscription#request-body
     * @return Subscription
     * @throws SquareupRequestException
     */
    public function create(array $data): Subscription;

    /**
     * Updates a webhook subscription.
     *
     * @param string $subscription_id The ID of the Subscription to update.
     * @param array $data For detail see https://developer.squareup.com/reference/square_2023-10-20/webhook-subscriptions-api/update-webhook-subscription#request-body
     * @return Subscription
     * @throws SquareupRequestException
     */
    public function update(string $subscription_id, array $data): Subscription;

    /**
     * Updates a webhook subscription by replacing the existing signature key with a new one.
     *
     * @param string $subscription_id The ID of the Subscription to update.
     * @param string $idempotency_key A unique string that identifies the UpdateWebhookSubscriptionSignatureKey request.
     * @return string signature_key
     * @throws SquareupRequestException
     */
    public function updateSignature(string $subscription_id, string $idempotency_key): string;

    /**
     * Deletes a webhook subscription.
     *
     * @param string $subscription_id The ID of the Subscription to delete.
     * @return void
     * @throws SquareupRequestException
     */
    public function delete(string $subscription_id): void;

    /**
     * Tests a webhook subscription by sending a test event to the notification URL.
     *
     * @param string $subscription_id The ID of the Subscription to test.
     * @param string $event_type The event type that will be used to test the Subscription. The event type must be contained in the list of event types in the Subscription.
     * @return TestSubscription
     * @throws SquareupRequestException
     */
    public function test(string $subscription_id, string $event_type): TestSubscription;
}
