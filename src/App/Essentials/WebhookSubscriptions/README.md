# Squareup Webhook Subscriptions

The Squareup Webhook Subscriptions API enables the creation, retrieval, update, and deletion of webhook subscriptions. It's essential to note that since webhook subscriptions are associated with the application and not a specific seller, you must use the application's personal access token, as OAuth access tokens are not applicable in this context. For detailed information, refer to the [Squareup API Documentation](https://developer.squareup.com/reference/square_2023-10-20/webhook-subscriptions-api).

## Usage

Utilize the following methods provided by the `WebhookSubs` facade to interact with the Squareup Webhook Subscriptions:

```php
use MahanShoghy\LaravelSquareup\Facades\WebhookSubs;

// List all the event types that can be subscribed to
WebhookSubs::listEvents($api_version): EventType;

// List all your webhook subscriptions
WebhookSubs::list($cursor, $include_disabled, $sort_order, $limit): Pagination;

// Retrieve a specific webhook subscription
WebhookSubs::retrieve($subscription_id): Subscription;

// Create a new webhook subscription
WebhookSubs::create($data): Subscription;

// Update an existing webhook subscription
WebhookSubs::update($subscription_id, $data): Subscription;

// Update the signature of a webhook subscription
WebhookSubs::updateSignature($subscription_id, $idempotency_key): string;

// Delete a webhook subscription
WebhookSubs::delete($subscription_id): void;

// Test a webhook subscription by simulating an event
WebhookSubs::test($subscription_id, $event_type): TestSubscription;
```
