<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data;

class Pagination
{
    public function __construct(
        public readonly string $cursor,
        public readonly array $subscriptions
    ){}
}
