<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class Subscription implements DataInterface
{
    public function __construct(
        public readonly ?string $id,
        public readonly ?string $name,
        public readonly bool $enabled,
        public readonly array $event_types,
        public readonly ?string $notification_url,
        public readonly ?string $api_version,
        public readonly ?Carbon $created_at,
        public readonly ?Carbon $updated_at
    ){}

    public static function fake(): static
    {
        return new static(
            "wbhk_b35f6b3145074cf9ad513610786c19d5",
            "Example Webhook Subscription",
            true,
            [
                "payment.created",
                "payment.updated"
            ],
            "https://example-webhook-url.com",
            "2021-12-15",
            Carbon::parse("2022-01-10 23:29:48 +0000 UTC"),
            Carbon::parse("2022-01-10 23:29:48 +0000 UTC")
        );
    }
}
