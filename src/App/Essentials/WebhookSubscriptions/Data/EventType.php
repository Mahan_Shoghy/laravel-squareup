<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data;

use MahanShoghy\LaravelSquareup\Interfaces\DataInterface;

class EventType implements DataInterface
{
    public function __construct(
        public readonly array $event_types,
        public readonly array $metadata
    ){}

    public static function fake(): static
    {
        return new static([
                'inventory.count.updated'
            ], [
                [
                    'event_type' => 'inventory.count.updated',
                    'api_version_introduced' => '2018-07-12',
                    'release_status' => 'PUBLIC'
                ]
            ],
        );
    }
}
