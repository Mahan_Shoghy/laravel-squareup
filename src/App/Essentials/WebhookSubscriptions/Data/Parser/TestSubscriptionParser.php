<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\TestSubscription;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class TestSubscriptionParser implements ParserInterface
{
    private TestSubscription $test_subscription;

    public function __construct(array $data)
    {
        $created_at = (empty($data['created_at'])) ? null
            : Carbon::parse($data['created_at']);

        $updated_at = (empty($data['updated_at'])) ? null
            : Carbon::parse($data['updated_at']);

        $this->test_subscription = new TestSubscription(
            $data['id'] ?? null,
            $data['status_code'] ?? null,
            $data['payload'] ?? null,
                $created_at,
                $updated_at,
        );
    }

    public function get(): TestSubscription
    {
        return $this->test_subscription;
    }
}
