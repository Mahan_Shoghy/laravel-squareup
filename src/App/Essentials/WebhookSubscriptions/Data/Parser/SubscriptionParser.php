<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser;

use Illuminate\Support\Carbon;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Subscription;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class SubscriptionParser implements ParserInterface
{
    private Subscription $subscription;

    public function __construct(array $data)
    {
        $created_at = (empty($data['created_at'])) ? null
            : Carbon::parse($data['created_at']);

        $updated_at = (empty($data['updated_at'])) ? null
            : Carbon::parse($data['updated_at']);

        $this->subscription = new Subscription(
            $data['id'] ?? null,
            $data['name'] ?? null,
            $data['enabled'],
            $data['event_types'] ?? [],
            $data['notification_url'] ?? null,
            $data['api_version'] ?? null,
                $created_at,
                $updated_at,
        );
    }

    public function get(): Subscription
    {
        return $this->subscription;
    }
}
