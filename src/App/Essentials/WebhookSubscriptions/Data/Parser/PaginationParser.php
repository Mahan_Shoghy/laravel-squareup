<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser;

use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Pagination;
use MahanShoghy\LaravelSquareup\Interfaces\ParserInterface;

class PaginationParser implements ParserInterface
{
    private Pagination $pagination;

    public function __construct(array $data)
    {
        $this->pagination = new Pagination(
            $data['cursor'],
            collect($data['subscriptions'])->map(function ($item){
                return new SubscriptionParser($item);
            })->toArray()
        );
    }

    public function get(): Pagination
    {
        return $this->pagination;
    }
}
