<?php

namespace MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions;

use MahanShoghy\LaravelSquareup\App\ApiProvider;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\EventType;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Pagination;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser\PaginationParser;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser\SubscriptionParser;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Parser\TestSubscriptionParser;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\Subscription;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Data\TestSubscription;
use MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\Interfaces\WebhookSubsInterface;

class WebhookSubs extends ApiProvider implements WebhookSubsInterface
{
    public function listEvents(?string $api_version = null): EventType
    {
        $query = [];
        if ($api_version){
            $query['api_version'] = $api_version;
        }
        $query = http_build_query($query);

       $response = $this->http->withToken(config('squareup.token'))
           ->get("$this->base_url/v2/webhooks/event-types?$query");

       $this->checkResponse($response);
       $response = json_decode($response->body(), true);

       return new EventType($response['event_types'], $response['metadata']);
    }

    public function list(?string $cursor = null, ?bool $include_disabled = null, ?string $sort_order = null, int $limit = 100): Pagination
    {
        $query = ['limit' => $limit];
        if ($cursor){
            $query['cursor'] = $cursor;
        }
        if ($include_disabled){
            $query['include_disabled'] = $include_disabled;
        }
        if ($sort_order){
            $query['sort_order'] = $sort_order;
        }
        $query = http_build_query($query);

        $response = $this->http->withToken(config('squareup.token'))
            ->get("$this->base_url/v2/webhooks/subscriptions?$query");

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new PaginationParser($response))->get();
    }

    public function retrieve(string $subscription_id): Subscription
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->get("$this->base_url/v2/webhooks/subscriptions/$subscription_id");

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new SubscriptionParser($response))->get();
    }

    public function create(array $data): Subscription
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->post("$this->base_url/v2/webhooks/subscriptions", $data);

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new SubscriptionParser($response))->get();
    }

    public function update(string $subscription_id, array $data): Subscription
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->put("$this->base_url/v2/webhooks/subscriptions/$subscription_id", $data);

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new SubscriptionParser($response))->get();
    }

    public function updateSignature(string $subscription_id, string $idempotency_key): string
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->post("$this->base_url/v2/webhooks/subscriptions/$subscription_id/signature-key", compact(['idempotency_key']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (string) $response['signature_key'];
    }

    public function delete(string $subscription_id): void
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->delete("$this->base_url/v2/webhooks/subscriptions/$subscription_id");

        $this->checkResponse($response);
    }

    public function test(string $subscription_id, string $event_type): TestSubscription
    {
        $response = $this->http->withToken(config('squareup.token'))
            ->post("$this->base_url/v2/webhooks/subscriptions/$subscription_id/test", compact(['event_type']));

        $this->checkResponse($response);
        $response = json_decode($response->body(), true);

        return (new TestSubscriptionParser($response))->get();
    }
}
