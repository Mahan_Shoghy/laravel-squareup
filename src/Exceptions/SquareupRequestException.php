<?php

namespace MahanShoghy\LaravelSquareup\Exceptions;

use Exception;
use Illuminate\Http\Client\Response;
use Throwable;

class SquareupRequestException extends Exception
{
    private array $errors = [];

    public function __construct(Response $response, ?Throwable $previous = null)
    {
        $body = $response->body();
        $body = json_decode($body, true);

        if (empty($body['errors'])){
            $message = "{$body['type']} : {$body['message']}";
        }
        else {
            $this->errors = $body['errors'];
            $body = $this->errors[0];

            $message = "{$body['category']} : {$body['code']}";

            if (!empty($body['field']) || !empty($body['detail'])){
                $message .= ' -';

                if (!empty($body['field'])){
                    $message .= ' '.$body['field'];
                }
                if (!empty($body['detail'])){
                    $message .= ' '.$body['detail'];
                }
            }
        }


        parent::__construct($message, $response->status(), $previous);
    }

    public function getErrors(): string
    {
        return $this->errors;
    }
}
