<?php

namespace MahanShoghy\LaravelSquareup\Interfaces;

interface ApiProviderInterface
{
    public function setToken(string $token): static;

    public function getToken(): ?string;
}
