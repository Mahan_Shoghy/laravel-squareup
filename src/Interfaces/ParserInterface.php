<?php

namespace MahanShoghy\LaravelSquareup\Interfaces;

interface ParserInterface
{
    public function __construct(array $data);

    public function get();
}
