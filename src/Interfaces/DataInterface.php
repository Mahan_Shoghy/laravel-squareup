<?php

namespace MahanShoghy\LaravelSquareup\Interfaces;

interface DataInterface
{
    /**
     * Return fake data of class
     */
    public static function fake(): static;
}
