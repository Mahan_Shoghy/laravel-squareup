<?php

return [
    // The Squareup Application ID obtained from your Developer Dashboard
    'id' => env('SQUAREUP_APPLICATION_ID'),

    // The Squareup Application Secret obtained from your Developer Dashboard
    'secret' => env('SQUAREUP_APPLICATION_SECRET'),

    // The Squareup Access Token obtained from your Developer Dashboard
    'token' => env('SQUAREUP_ACCESS_TOKEN'),

    /**
     * The base URL for Squareup API requests.
     * Choose the appropriate URL based on whether you are using the main or sandbox environment.
     * main: https://connect.squareup.com
     * sandbox: https://connect.squareupsandbox.com
     */
    'hostname' => env('SQUAREUP_HOST', 'https://connect.squareup.com'),

    'oauth' => [
        /**
         * List of permissions that the application is requesting.
         * Include the necessary scopes for your application to access Squareup resources.
         * Refer to the Squareup documentation for available scopes.
         * More information: https://developer.squareup.com/reference/square_2023-10-20/o-auth-api/authorize#query__property-scope
         */
        'scopes' => [
            'ITEMS_WRITE',
            'CUSTOMERS_READ',
            // Add more scopes as required
        ],

        // The locale to present the permission request form in
        'locale' => 'en-US',

        // Define whether the user needs to log in to their Square account to view the Permission Request form
        'session' => true,

        // Use 'state' to protect against cross-site request forgery
        'state' => true,

        // The redirect URL assigned on the OAuth page for your application in the Developer Dashboard
        'redirect_uri' => env('APP_URL') . '/squareup/redirect_uri'
    ],

    'webhook' => [
        // List of valid IPs that can send requests through the webhook
        'ips' => [
            // Square production IPs
            '54.245.1.154',
            '34.202.99.168',
            // Square sandbox IPs
            '54.212.177.79',
            '107.20.218.8'
        ],

        /**
         * List of jobs that will handle webhook requests
         * Please use (underline) instead of (dots) when defining the job keys.
         * You can refer to the full list of webhooks at
         * https://developer.squareup.com/reference/square/webhooks
         *
         * Example: 'bank_account_created' => BankAccountCreatedJob::class
         */
        'jobs' => [
            // Jobs can be added here
        ]
    ]
];
