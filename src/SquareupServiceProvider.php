<?php

namespace MahanShoghy\LaravelSquareup;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use MahanShoghy\LaravelSquareup\App\Webhook\Middlewares\IpMiddleware;
use MahanShoghy\LaravelSquareup\App\Webhook\WebhookController;


class SquareupServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/config.php', 'squareup');

        $this->registerFacades();
        $this->registerWebhook();
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/config.php' => config_path('squareup.php')
            ], 'config');
        }
    }

    private function registerFacades(): void
    {
        $facades = [
            'OAuth' => [
                'facade' => \MahanShoghy\LaravelSquareup\Facades\OAuth::class,
                'main' => \MahanShoghy\LaravelSquareup\App\Essentials\OAuth\OAuth::class
            ],
            'WebhookSubs' => [
                'facade' => \MahanShoghy\LaravelSquareup\Facades\WebhookSubs::class,
                'main' => \MahanShoghy\LaravelSquareup\App\Essentials\WebhookSubscriptions\WebhookSubs::class
            ],
            'Orders' => [
                'facade' => \MahanShoghy\LaravelSquareup\Facades\Orders::class,
                'main' => \MahanShoghy\LaravelSquareup\App\Commerce\Orders\Orders::class
            ],
            'Locations' => [
                'facade' => \MahanShoghy\LaravelSquareup\Facades\Locations::class,
                'main' => \MahanShoghy\LaravelSquareup\App\Merchants\Locations\Locations::class
            ]
        ];

        foreach ($facades as $name => $class){
            $this->app->bind($name, $class['main']);

            $loader = AliasLoader::getInstance();
            $loader->alias($name, $class['facade']);
        }
    }

    private function registerWebhook(): void
    {
        Route::macro('squareupWebhooks', function (string $url) {
            return Route::get($url, WebhookController::class)->middleware(IpMiddleware::class);
        });
    }
}
